﻿using System;
using System.Collections.Generic;

public class Program
{
    public static void Main(string[] args)
    {
        Console.WriteLine("String bracket:");
        string input = Console.ReadLine();

        string result = CheckBalance(input) ? "YES" : "NO";

        Console.WriteLine($"Output: {result}");
    }

    public static bool CheckBalance(string s)
    {
        Stack<char> arr = new Stack<char>();

        foreach (char c in s)
        {
            if (c == '{' || c == '[' || c == '(')
            {
                arr.Push(c);
            }
            else if (c == '}' || c == ']' || c == ')')
            {
                if (arr.Count == 0)
                {
                    return false;
                }

                char top = arr.Pop();
                if (!CheckMatching(top, c))
                {
                    return false;
                }
            }
        }

        return arr.Count == 0;
    }

    public static bool CheckMatching(char awal, char akhir)
    {
        return (awal == '{' && akhir == '}') ||
               (awal == '[' && akhir == ']') ||
               (awal == '(' && akhir == ')');
    }
}
