﻿using System;
using System.Collections.Generic;

public class Program
{
    public static void Main(string[] args)
    {
        Console.WriteLine("Please input string");
        string input = Console.ReadLine();

        Console.WriteLine("Queries with space:");
        string[] qInput = Console.ReadLine().Split();
        int[] qr = Array.ConvertAll(qInput, int.Parse);

        List<string> results = ProcessWeightedStrings(input, qr);

        foreach (var result in results)
        {
            Console.WriteLine(result);
        }
    }

    public static List<string> ProcessWeightedStrings(string s, int[] qr)
    {
        int i = 0;
        HashSet<int> berat = new HashSet<int>();
        List<string> result = new List<string>();

       
        while (i < s.Length)
        {
            char curChar = s[i];
            int br = CharBerat(curChar);
            int currentWeight = 0;

            while (i < s.Length && s[i] == curChar)
            {
                currentWeight += br;
                berat.Add(currentWeight);
                i++;
            }
        }
       
        foreach (int query in qr)
        {
            if (berat.Contains(query))
            {
                result.Add("Yes");
            }
            else
            {
                result.Add("No");
            }
        }

        return result;
    }

    public static int CharBerat(char c)
    {
        return c - 'a' + 1;
    }
}
