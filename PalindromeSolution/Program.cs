﻿using System;

public class Program
{
    public static void Main(string[] args)
    {
        Console.WriteLine("please input number:");
        string input = Console.ReadLine();

        Console.WriteLine("input allowed replacement (k):");
        int k = int.Parse(Console.ReadLine());

        string result = CheckHighest(input, k);
        Console.WriteLine($"Output: {result}");
    }

    public static string CheckHighest(string s, int k)
    {
        char[] chars = s.ToCharArray();
        if (CheckPalindrome(chars, 0, chars.Length - 1, ref k))
        {
            return new string(chars);
        }
        return "-1";
    }

    public static bool CheckPalindrome (char[] chars, int a, int b, ref int k)
    {
        char baseNum = '9'; //9 karena hanya ada 9 char angka berbeda dalam matdas
        if (a >= b) 
            return true;

        if (chars[a] != chars[b])
        {
            char maxChar = chars[a] > chars[b] ? chars[a] : chars[b];
            chars[a] = maxChar;
            chars[b] = maxChar;
            k--;
        }

        if (k < 0) 
            return false;

        bool isPali = CheckPalindrome(chars, a + 1, b - 1, ref k);

        if (isPali && k > 0 && chars[a] != baseNum) 
        {
            if (a != b)
            {
                chars[a] = baseNum;
                chars[b] = baseNum;
                k -= 2;
            }
            else
            {
                chars[a] = baseNum;
                k--;
            }
        }

        return isPali;
    }
}
